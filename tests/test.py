#!/usr/bin/env python3
import sys
from pathlib import Path
import unittest
import warnings
from functools import partial
thisDir = Path(__file__).parent.absolute()
sys.path.append(str(thisDir.parent))

from datag.core.actions import *

class TestActions(unittest.TestCase):
	recKey = "a"
	testRec = {recKey: "1 cm"}
	actionsTests = (
		(ReplaceStr, ("c", "d"), testRec, {recKey: "1 dm"}),
		(ReplaceRX, ("c+", "d", testRec,  {recKey: "1 dm"}),
		(Apply, (lambda x: x.replace("c", "d"),), testRec,  {recKey: "1 dm"}),
		(Update, (lambda x: {(recKey+"_"): x.replace("c", "m")},), testRec, {recKey: "1 cm", (recKey+"_"):"1 mm"}),
		(Rename, ("b",), testRec,  {"b": "1 cm"}),
		(Drop, (), testRec, {}),
		(Parse, (), testRec, {recKey: ureg.cm*1}),
		
		(ActionAdapter, (), testRec, testRec),
		#(ActionAdapter, (*a: typing.Iterable[FixerFT]), {}),
		#(ID, (*a: typing.Iterable[FixerFT]), testRec, Record(ID=("1 cm",))),
		(Version, (groups:int=1, delimiter:str="."), {recKey: "1.2.3"}, {recKey: (1,2,3)}),
		(ApplyUnit, (unit: ureg.cm), {recKey: 1}, {recKey: ureg.cm*1}),
	)
	def testActions(self):
		for actionCtor, args, testRec, etalonRes in self.__class__.actionsTests():
			action = actionCtor(*args)
			testRec = type(testRec)(testRec)
			resRec = action(None, testRec, self.__class__.recKey)
			self.assertEquals(resRec, etalonRec)

if __name__ == '__main__':
	unittest.main()